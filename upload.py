import os
import io
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

# Path to your service account key file
SERVICE_ACCOUNT_FILE = 'path/to/your/service-account-file.json'

# Authenticate using the service account
credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE,
    scopes=['https://www.googleapis.com/auth/drive.file']
)

# Create a Google Drive service instance
service = build('drive', 'v3', credentials=credentials)

def upload_file(file_path, file_name):
    # Define the file metadata
    file_metadata = {'name': file_name}

    # Create a MediaFileUpload object for the file
    media = MediaFileUpload(file_path, mimetype='application/octet-stream')

    # Upload the file
    file = service.files().create(
        body=file_metadata,
        media_body=media,
        fields='id'
    ).execute()

    print(f'File ID: {file.get("id")}')

# Example usage
if __name__ == '__main__':
    # Path to the file you want to upload
    file_path = 'path/to/your/file.txt'

    # Name to be given to the file on Google Drive
    file_name = 'uploaded_file.txt'

    upload_file(file_path, file_name)

